#!/bin/sh
# Script by Ypnose - http://ywstd.fr
set -e

# For a better efficiency, this script should be called everyday, using
# /etc/crontab or any other cron-like mechanism
# m  h  dom mon dow user    command
# 00 05 *   *   *   lambda  rypp-backup /var/www/repo

####################

RYPP_UTIL="${0##*/}"
RYPP_WEBD="$1"
# Works with date(1), BusyBox date(1) and OpenBSD date(1)
RYPP_SNAP="$(date "+%Y-%m-%d")"
RYPP_ROTA="5"
LC_ALL=C
export LC_ALL

####################

p_err() {
	printf "ERR: %s\n" "$1" >&2
	exit 1
}

gen_sna() {
	mkdir -p snapshot
	cp RYPPLIST SHA256SUM* *.rypp.tgz snapshot
}

####################

if [ -z "$RYPP_WEBD" ] || [ "$1" = "-h" ]; then
	printf "%s\n" "usage: $RYPP_UTIL [REPODIR]"
	exit
elif [ ! -d "$RYPP_WEBD" ]; then
	p_err "$RYPP_WEBD directory is missing"
fi

cd "$RYPP_WEBD"

# If snapshot is present, compare it with the current repository
if [ -d snapshot ]; then
	for r in snapshot/RYPPLIST RYPPLIST; do
		if [ ! -r "$r" ]; then
			p_err "$r is either missing or unreadable"
		fi
	done
	if ! diff -rq snapshot/RYPPLIST RYPPLIST >/dev/null 2>&1; then
		# Remove an already existing RYPP_SNAP directory
		if [ -d "$RYPP_SNAP" ]; then
			rm -r "$RYPP_SNAP"
		fi
		mv snapshot "$RYPP_SNAP"
		gen_sna
	fi
else
	# The first time the script is launched, it'll create the snapshot
	gen_sna
fi

# Minimalistic counter without tail(1) for backup rotation
n=1
find . -maxdepth 1 -type d -name "201*" | sort -rn \
	| while IFS=$'\n' read -r RT_DIR; do
		if [ "$n" -gt "$RYPP_ROTA" ]; then
			rm -r "$RT_DIR"
		else
			n="$(( n + 1 ))"
		fi
done

exit
