rypp - [r]ugged [y]pnose's [p]ackage [p]uller
=============================================

Another tool to grab your essential package(s) without having to
learn / care about the distribution you use. The packages will be
statically built, using musl libc.

## Why?

 * Highly portable (it will most likely work on other Unix-like systems)
 * Binaries statically linked against [musl](https://www.musl-libc.org/) libc
 * Software running on every Linux distribution
 * Compile softwares with only the options you want / need
 * Do not rely on maintainers and avoid their obscure and brainless choices
 * Do not learn some stupid and irrelevant dialects to create packages
 * Use the same versions on all your boxes
 * Signed archives using ```signify(1)``` from OpenBSD
 * Softwares built with ```-fstack-protector-strong```

## Goals

 * Easy to use and portable (POSIX ```sh```)
 * DragonFly BSD support?

## Usage

```rypp``` is a generic name. It will provide some standalone scripts.
For a basic use, only ```rypp``` will be enough.

 - ```rypp```        - to install them on your box(es).
 - ```rypp-backup``` - to create repository snapshots.
 - ```rypp-mk```     - to compile and create package(s).
 - ```rypp-repo```   - to create the final repository.

rypp utilities do not use any configuration files. The user-defined
variables must be set in the environment, using ```export```. It can be
automatically done with user profile, like ```${HOME}/.profile```
or ```${HOME}/.bash_profile```.

	$ export RYPP_DEST="${HOME}/rypp"

For a list of possible parameters, read the sources.

## Notes

If you use ```rypp```, please drop me an email. I would love to know
someone who uses it.

## Author

Ypnose - http://ywstd.fr/

## License

BSD 3-Clause License. Check LICENSE.
